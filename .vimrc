" ********************************************************************
"                    GENERAL VIM CONFIGURATIONS
" ********************************************************************

filetype plugin indent on
syntax on
set guifont=Hack\ 12
set encoding=UTF-8
set autoindent                  " take indent for new line from previous line
set shiftwidth=4                " number of spaces to use for (auto)indent step
set smartindent                 " smart autoindenting for C programs
set expandtab                   " use spaces when <Tab> is inserted
set smarttab                    " use 'shiftwidth' when inserting <Tab>
set softtabstop=4               " number of spaces that <Tab> uses while editing
set tabstop=4                   " number of spaces that <Tab> in file uses
set number                      " print the line number in front of each line
set numberwidth=4               " number of columns used for the line number
set splitright                  " new window is put right of the current one
set noswapfile                  " whether to use a swapfile for a buffer
set nobackup                    " do not create a backup file on each save "
set nowritebackup               " do not backup on write
set backspace=indent,eol,start  " how backspace works at start of line
set hlsearch                    " highlight matches with last search pattern
set ignorecase                  " ignore case in search patterns
set incsearch                   " highlight match while typing search pattern
set smartcase                   " no ignore case when pattern has uppercase
set laststatus=2                " correctly display lightline
set encoding=UTF-8              " set correct encoding
set redrawtime=10000            " in large files prevent syntax issues
set splitright                  " split window right
set splitbelow                  " split window below
set nowrap                      " disable line wrapping
set mouse=a                     " activate mouse

" let g:loaded_matchparen = 1     " disable matching parentheses (its confusing at some themes)
hi MatchParen ctermbg=blue guibg=lightblue

if has("clipboard")
  set clipboard=unnamed,unnamedplus
endif

" enhance scrolling times:e
set cursorline!
set regexpengine=1
set ttyfast

" Related to coc.nvim extension
set hidden " textedit in coc.nvim might fail if hidden is not set
set cmdheight=2 " give more space for displaying messages
set updatetime=300 " better UX, shorter update time than default
set shortmess+=c " dont pass messages to  ins-completion menu
set showtabline=1  " always show tabline
set signcolumn=yes
set noshowmode " disable duplicitly showing state eg. INSERT, VISUAL

" ********************************************************************
"                     VIM PLUG plugin manager
" ********************************************************************

call plug#begin()

" helpers
Plug 'preservim/NERDTree'
Plug 'sheerun/vim-polyglot'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'luochen1990/rainbow'
Plug 'ryanoasis/vim-devicons'
Plug 'Yggdroot/indentLine'
Plug 'puremourning/vimspector'
Plug 'luochen1990/rainbow'

" RUST plugins
Plug 'rust-lang/rust.vim'
Plug 'cespare/vim-toml'

" Themes
Plug 'rakr/vim-one'
Plug 'glepnir/oceanic-material'
Plug 'w0ng/vim-hybrid'
Plug 'adrian5/oceanic-next-vim'
Plug 'tyrannicaltoucan/vim-quantum'
Plug 'sonph/onehalf', { 'rtp': 'vim' }
Plug 'joshdick/onedark.vim'

" lightline
Plug 'itchyny/lightline.vim'

call plug#end()

" ********************************************************************
"                     PLUGIN SPECIFIC SETTINGS
" ********************************************************************

let NERDTreeShowHidden = 1

" switch between autocomplete options in popup
inoremap <silent><expr> <Tab> pumvisible() ? "\<C-n>" : "\<TAB>"

" select autocompleted on enter
:inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" use [g and ]g to navigate diagnostics
" use :CocDiagnostics to get all diagnostics of current buffer in loc list
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" highlight the symbol and its referencs when holding the cursor
autocmd Cursorhold * silent call CocActionAsync('highlight')

" symbol renaming
nmap <leader>rn <Plug>(coc-rename)

" apply AutoFix to problem on the current line
nmap <leader>qf <Plug>(coc-fix-current)


" allways enable termdebugger for instance of vim
packadd termdebug

" term debugger for RUST
let g:termdebugger="rust-gdb"

" activate rainbow parentheses in the need with RainbowToggle
let g:rainbow_active=0

" ********************************************************************
"                         UI AND THEMES
" ********************************************************************

" for vim 8
if (has("termguicolors"))
  set termguicolors
endif

" Oceanic Material theme settings
let g:oceanic_material_transparent_background = 1
let g:oceanic_material_allow_underline = 0
let g:oceanic_material_allow_undercurl = 1

" Quantum theme settings
let g:quantum_black = 1
let g:quantum_italics = 1

" One theme settings
let g:one_allow_italics = 0

" COLORSCHEME APPLIED
set background=dark
colorscheme quantum " quantum


" Activation of rainbow parentheses based on file type
let g:rainbow_active = 0 "set to 0 if you want to enable it later via :RainbowToggle
"
" turn off rainbow parentheses in nerdtree
let g:rainbow_conf = {
	\	'separately': {
	\		'nerdtree': 0,
	\	}
	\}

hi Normal guibg=#212121 ctermbg=NONE
hi LineNr guifg=#737373 guibg=NONE ctermbg=NONE
hi SignColumn guibg=NONE ctermbg=NONE
hi EndOfBuffer ctermbg=NONE ctermfg=NONE guibg=#212121 guifg=#212121
set cursorline
hi CursorLine guibg=Grey25
hi CursorLineNr guibg=Grey25
hi Comment guifg=#919c9c

" auto format on save in rust lang"
let g:rustfmt_autosave = 1


" ********************************************************************
"                         KEYBOARD SHORTCUTS
" ********************************************************************


let mapleader = "," " default is \ backslash
nmap <F5> :NERDTreeToggle<CR>

" refresh nerd tree window to show new files
nmap <Leader>r :NERDTreeRefreshRoot<CR>

" previous and next buffer switch
nmap <F2> :bp<CR>
nmap <F3> :bn<CR>

" open terminal in vim's current directory
map <F6> :let $VIM_DIR=expand('%:p:h')<CR>:terminal bash -c "cd $VIM_DIR;bash"<CR>

" show buffers "
nnorema <F8> :buffers<CR>:buffer<Space>

" remove trailing whitespace on save
autocmd BufWritePre * :%s/\s\+$//e

" save, close all buffers and quit vim instantly
nmap <C-q> :qa!<CR>

" save a buffer
nmap <C-s> :w<CR>

" save all buffers
nmap <C><A>s :wa<CR>

" run JS file in node
nmap <F7> :call Run() <cr>
function Run()
  exec "! node %"
endfunction

" ********************************************************************
"                        LIGHTLINE SETTINGS
" ********************************************************************

" prev scheme: 'quantum'
let g:lightline = {
      \ 'colorscheme': 'quantum',
      \ }

" use autocmd to force lightline update
autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()

" coc improvements of brackets on enter
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
				\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

